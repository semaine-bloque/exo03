package net.tncy.damien.myproject1;

public class Calculator {
  private int a, b;
  
  // une méthode pour effectuer une division avec 2 opérandes de type int.
  public Calculator(int a, int b) {
    this.a = a;
    this.b = b;
  }

  public float divide() {
    float x = this.a/this.b;
    System.out.println(x);
    return x;
  }
}
